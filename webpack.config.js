var path = require('path');
var webpack = require('webpack');

var ExtractTextPlugin = require('extract-text-webpack-plugin');

var SOURCE_DIR = path.resolve(__dirname, 'app/src');
var SASS_DIR = path.resolve(__dirname, 'app/scss');
var BUILD_DIR = path.resolve(__dirname, 'build');

var config = {
  entry: SOURCE_DIR + '/main.jsx',

  output: {
    path: BUILD_DIR,
    publicPath: '/build',
    filename: 'app.js',
  },

  devServer: {
    inline: true,
    port: 8080
  },

  module: {
    loaders: [{
      test: /\.jsx?$/,
      exclude: /node_modules/,
      loader: 'babel',

      query: {
         presets: ['es2015', 'react']
      }
    },
    ,
    {
      test: /\.scss$/,
      include: SASS_DIR,
      loader: ExtractTextPlugin.extract("style-loader", "css-loader!autoprefixer-loader!sass-loader")
    }]
  },
  plugins: [
      new ExtractTextPlugin("style.css", {allChunks: false})
  ],
  resolve: {
    extensions: ['', '.js', '.jsx', '.es6'],
  }
}

module.exports = config;
