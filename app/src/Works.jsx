import React from 'react';
import ScrollAnim from 'rc-scroll-anim';
import TweenOne from 'rc-tween-one';

var Element = ScrollAnim.Element;
var ScrollOverPack = ScrollAnim.OverPack;

require('../scss/works.scss');

class Works extends React.Component {
  get works() {
    return  [
              // {
              //   title: 'Loft',
              //   image: 'images/work-loft.jpg',
              //   screenImage: 'images/work-screen-loft.png',
              //   description: 'An e-commerce web site for Loft department store.',
              //   link: 'http://www.loftbangkok.com'
              // },
              // {
              //   title: 'Rakluke',
              //   image: 'images/work-rakluke.jpg',
              //   screenImage: 'images/work-screen-rakluke.png',
              //   description: 'A parent community web site.',
              //   link: 'http://www.rakluke.com'
              // },
              // {
              //   title: 'Paradise Park',
              //   image: 'images/work-paradisepark.jpg',
              //   screenImage: 'images/work-screen-paradisepark.png',
              //   description: 'A Thailand shopping mall web site.',
              //   link: 'http://www.paradisepark.co.th'
              // },
              // {
              //   title: 'SCG Heim',
              //   image: 'images/work-scgheim.jpg',
              //   screenImage: 'images/work-screen-scgheim.png',
              //   description: 'The most advanced home builder in Thailand.',
              //   link: 'http://www.scgheim.com'
              // },
              {
                title: 'Digital OTOP',
                image: 'images/work-digital-otop.jpg',
                screenImage: 'images/work-screen-digital-otop.png',
                description: 'A digital website for promoting OTOP products.',
                link: 'http://www.digitalotop.com'
              },
              {
                title: 'SNAP',
                image: 'images/work-snap.jpg',
                screenImage: 'images/work-screen-snap.png',
                description: 'A Sago Network for Asia and Pacific of UNFAO.',
                link: 'http://www.gosago.net'
              },
              {
                title: 'Waggingtail Bakery',
                image: 'images/work-waggingtailbkk.jpg',
                screenImage: 'images/work-screen-waggingtailbkk.png',
                description: 'A pet bakery shop based in Bangkok, Thailand.',
                link: 'http://www.waggingtailbkk.com'
              },
              {
                title: 'Natural Tale',
                image: 'images/work-naturaltale.jpg',
                screenImage: 'images/work-screen-naturaltale.png',
                description: 'A pet treats, toys, and accessories shop based in Bangkok, Thailand.',
                link: 'http://www.waggingtailbkk.com'
              }
            ]
  }
  render() {
    return (
      <Element scrollName="works" id="works">
        <ScrollOverPack always={false}>
          <WorksContent works={this.works} />
        </ScrollOverPack>

        <WorksContent works={this.works} id="works-shadow" className="shadow" />
      </Element>
    );
  }
}

module.exports = Works;

class WorksContent extends React.Component {
  render() {
    return (
      <div id={this.props.id} className={this.props.className}>
        <TweenOne animation={{ y: 0, opacity: 1, delay: 200, duration: 600 }} style={{ transform: 'translateY(100px)', opacity: 0 }} className="headline">
          SHOWCASE
        </TweenOne>

        <TweenOne animation={{ y: 0, opacity: 1, delay: 400, duration: 600 }} style={{ transform: 'translateY(100px)', opacity: 0 }} className="sub-headline">
          With many years of experience in web development, I've designed and developed a lots of website to meet different business requirements.
        </TweenOne>

        <TweenOne animation={{ y: 0, opacity: 1, delay: 600, duration: 600 }} style={{ transform: 'translateY(100px)', opacity: 0 }} className="sub-headline">
          <div className="topic-hr"></div>
        </TweenOne>

        <div className="works columns is-mobile is-multiline">
          {this.props.works.map(function(workItem, index){
            return (
              <WorkItem work={workItem} key={index} reactKey={index} />
            )
          })}
        </div>
      </div>
    )
  }
}

class WorkItem extends React.Component {
  render() {
    return (
      <TweenOne animation={{ opacity: 1, delay: 800 + (this.props.reactKey * 200), duration: 600 }} style={{ opacity: 0, backgroundImage: 'url(' + this.props.work.image + ')' }} className="work-item column is-half-mobile is-one-quarter-tablet">
        <div className="text">
          <div className="work-topic">{this.props.work.title}</div>
          <div className="work-description">{this.props.work.description}</div>
          <img src={this.props.work.screenImage} alt={this.props.work.title} className="work-image"/>
        </div>
      </TweenOne>
    )
  }
}
