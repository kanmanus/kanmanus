import React from 'react';
import ScrollAnim from 'rc-scroll-anim';
import TweenOne from 'rc-tween-one';

var Element = ScrollAnim.Element;
var ScrollOverPack = ScrollAnim.OverPack;

require('../scss/blogs.scss');

class Blogs extends React.Component {
  get blogs() {
    return  [
              {
                title: 'เมื่อเรากำลังจะเข้าสู่ยุคของ \'Agile\'',
                image: 'https://cdn-images-1.medium.com/max/800/1*A1y3Wi6VWd1XLMJHg53jHQ.png',
                content: '<p>หลายคนคงได้ยินคำว่า Agile กันมานานแล้ว.. แน่นอนครับมันเป็น methodology ของการทำงานที่ถือกำเนิดขึ้นตั้งแต่ปี 2001 นู่นน</p>'
                        + '<p>เกริ่นก่อนว่าตอนนี้มีโอกาสได้มาช่วยงานในบริษัทของน้า โดยรวมแล้วงานส่วนใหญ่จะเป็น Application development (ทั้ง web และ mobile application) ด้วยความที่องค์กรมีขนาดไม่ใหญ่มาก ส่วนการทำงานก็เป็นแบบ waterfall model ทำให้เกิดปัญหาเรื่องของ \'เวลา\'...</p>'
              }
            ]
  }

  render() {
    return (
      <Element scrollName="blogs" id="blogs">
        <ScrollOverPack always={false}>
          <BlogsContent blogs={this.blogs} />
        </ScrollOverPack>

        <BlogsContent blogs={this.blogs} id="blogs-shadow" className="shadow" />
      </Element>
    );
  }
}

module.exports = Blogs;

class BlogsContent extends React.Component {
  render() {
    return (
      <div id={this.props.id} className={this.props.className}>
        <TweenOne animation={{ y: 0, opacity: 1, delay: 200, duration: 600 }} style={{ transform: 'translateY(-100px)', opacity: 0 }} className="headline">
          MY BLOGS
        </TweenOne>

        <TweenOne animation={{ y: 0, opacity: 1, duration: 600 }} style={{ transform: 'translateY(-100px)', opacity: 0 }}>
          <div className="topic-hr"></div>
        </TweenOne>

        <TweenOne animation={{ y: 0, opacity: 1, delay: 400, duration: 600 }} style={{ transform: 'translateY(100px)', opacity: 0 }} className="blogs container">
          {this.props.blogs.map(function(blogItem, index){
            return <BlogItem blog={blogItem} key={index} />
          })}
        </TweenOne>

        <div className="view-all">
          <TweenOne animation={{ y: 0, opacity: 1, delay: 600, duration: 600 }} style={{ transform: 'translateY(100px)', opacity: 0 }} className="text dot">
            . . .
          </TweenOne>

          <TweenOne animation={{ y: 0, opacity: 1, delay: 800, duration: 600 }} style={{ transform: 'translateY(100px)', opacity: 0 }} className="text">
            Wanna read more?
          </TweenOne>

          <TweenOne animation={{ y: 0, opacity: 1, delay: 1000, duration: 600 }} style={{ transform: 'translateY(100px)', opacity: 0 }} className="text">
            <a href="https://medium.com/@kanmanus" target="_blank" className="btn-view-all">View All My Posts</a>
          </TweenOne>
        </div>
      </div>
    )
  }
}

class BlogItem extends React.Component {
  render() {
    let style = {backgroundImage: 'url(' + this.props.blog.image + ')'};

    return (
      <div className="blog-item columns is-gapless">
        <div className="blog-image column is-half-tablet" style={style}></div>
        <div className="blog-info column is-half-tablet">
          <div className="blog-title">{this.props.blog.title}</div>
          <div className="blog-content" dangerouslySetInnerHTML={{ __html: this.props.blog.content }}></div>
          <a href="https://medium.com/@kanmanus/%E0%B9%80%E0%B8%A1%E0%B8%B7%E0%B9%88%E0%B8%AD%E0%B9%80%E0%B8%A3%E0%B8%B2%E0%B8%81%E0%B8%B3%E0%B8%A5%E0%B8%B1%E0%B8%87%E0%B8%88%E0%B8%B0%E0%B9%80%E0%B8%82%E0%B9%89%E0%B8%B2%E0%B8%AA%E0%B8%B9%E0%B9%88%E0%B8%A2%E0%B8%B8%E0%B8%84%E0%B8%82%E0%B8%AD%E0%B8%87-agile-7f04a6ab554d#.z8u9kfpts" target="_blank" className="btn-read-more">Read More</a>

          <div className="published-via">
            <div className="text">published via</div>
            <div className="logo">
              <img src="images/blog-logo-medium.png" alt="Medium"/>
            </div>
          </div>
        </div>
      </div>
    )
  }
}
