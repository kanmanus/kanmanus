import React from 'react';
import ReactDOM from 'react-dom';
import ScrollAnim from 'rc-scroll-anim';
import TweenOne from 'rc-tween-one';

var Link = ScrollAnim.Link;

class Header extends React.Component {
  constructor(props){
    super(props);

    this.state = {
      headerSize: '',
      lastNavItemClasses: 'nav-item',
      navContent: []
    }

    this.handleScroll = this.handleScroll.bind(this)

    var navElements = [];
    navElements.push(<Link className="nav-item" active="active" location="home" duration={500} offset={-84} key="1">HOME</Link>)
    navElements.push(<Link className="nav-item" active="active" location="profile" duration={500} offset={-84} onFocus={this.props.focusEvent} key="2">PROFILE</Link>)
    navElements.push(<Link className="nav-item" active="active" location="skills" duration={500} offset={-84} onFocus={this.props.focusEvent} key="3">SKILLS</Link>)
    navElements.push(<Link className="nav-item" active="active" location="works" duration={500} offset={-84} onFocus={this.props.focusEvent} key="4">WORKS</Link>)
    navElements.push(<Link className="nav-item" active="active" location="blogs" duration={500} offset={-83} onFocus={this.props.focusEvent} key="5">BLOGS</Link>)

    var component = this;
    setTimeout(function() {
      component.setState({
        navContent: navElements
      })
    }, 5100);
  }

  componentDidMount(){
    window.addEventListener('scroll', this.handleScroll)
  }

  componentWillUnmount(){
    window.removeEventListener('scroll', this.handleScroll)
  }

  handleScroll(event){
    let scrollTop = event.srcElement.body.scrollTop;

    this.setState({
      headerSize: (scrollTop > 0)?'sm':''
    })
  }

  render() {
    return (
      <header className={this.state.headerSize} style={this.props.style}>
        <TweenOne animation={{ y: 0, opacity: 1, delay: 5000 }} style={{ transform: 'translateY(100px)', opacity: 0 }}>
          <Link id="logo" location="home" duration={500} offset={-84}>
            <img src="images/logo.png" alt="Kanmanus" />
          </Link>

          <nav>
            {this.state.navContent}
          </nav>

          <div id="nav-toggle">
            <img src="images/nav-toggle-icon.png" onClick={this.props.toggleEvent.bind(this)} />
          </div>
        </TweenOne>
      </header>
    );
  }
}


module.exports = Header;
