import React from 'react';
import ScrollAnim from 'rc-scroll-anim';

var Link = ScrollAnim.Link;

require('../scss/global/sidebar.scss');

class SideBar extends React.Component {
  constructor(props) {
    super(props)

    this.state = {
      navContent: []
    }

    var navElements = [];
    navElements.push(<Link className="nav-item" active="active" location="home" duration={500} offset={-84} onClick={this.props.sideBarNavEvent} key="0">HOME</Link>)
    navElements.push(<Link className="nav-item" active="active" location="profile" duration={500} offset={-84} onClick={this.props.sideBarNavEvent} onFocus={this.props.focusEvent} key="1">PROFILE</Link>)
    navElements.push(<Link className="nav-item" active="active" location="skills" duration={500} offset={-84} onClick={this.props.sideBarNavEvent} onFocus={this.props.focusEvent} key="2">SKILLS</Link>)
    navElements.push(<Link className="nav-item" active="active" location="works" duration={500} offset={-84} onClick={this.props.sideBarNavEvent} onFocus={this.props.focusEvent} key="3">WORKS</Link>)
    navElements.push(<Link className="nav-item" active="active" location="blogs" duration={500} offset={-83} onClick={this.props.sideBarNavEvent} onFocus={this.props.focusEvent} key="4">BLOGS</Link>)

    var component = this;
    setTimeout(function() {
      component.setState({
        navContent: navElements
      })
    }, 5100);
  }

  render() {
    return (
      <div id="sidebar" style={{ transform: (this.props.isShow?'translateX(0)':'translateX(100%)') }}>
        <div className="btn-close" onClick={this.props.toggleEvent.bind(this)}>X</div>

        {this.state.navContent}
      </div>
    );
  }
}


module.exports = SideBar;
