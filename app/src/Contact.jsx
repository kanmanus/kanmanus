import React from 'react';
import {Parallax} from 'react-parallax';
import Scroll from 'react-scroll';
import TweenOne from 'rc-tween-one';

var Element = Scroll.Element;

require('../scss/contact.scss');

class Contact extends React.Component {
  render() {
    return (
      <Element name="contact" id="contact">
        <Parallax bgImage="images/contact-banner.jpg" strength={400}>
          <div className="text">
            <TweenOne animation={{ y: 0, opacity: 1, delay: 200, duration: 600 }} style={{ transform: 'translateY(-100px)', opacity: 0 }} className="headline">
              INTERESTED IN MY WORKS?
            </TweenOne>

            <TweenOne animation={{ y: 0, opacity: 1, duration: 600 }} style={{ transform: 'translateY(-100px)', opacity: 0 }}>
              <div className="topic-hr"></div>
            </TweenOne>

            <TweenOne animation={{ y: 0, opacity: 1, delay: 400, duration: 600 }} style={{ transform: 'translateY(100px)', opacity: 0 }} className="sub-headline">
              Don't hesitate to contact me now! I'm ready to bring you an awesome website.
            </TweenOne>

            <div className="contact-bar">
              <TweenOne animation={{ y: 0, opacity: 1, delay: 600, duration: 600 }} style={{ transform: 'translateY(20px)', opacity: 0 }}>
                <a href="mailto:kanmanus.o@gmail.com" className="contact-item">
                  <img src="images/contact-icon-email.png" alt="E-mail" />
                </a>

                <a href="http://www.facebook.com/kanmanus.ong" target="_blank" className="contact-item">
                  <img src="images/contact-icon-fb.png" alt="Facebook"/>
                </a>

                <a href="https://th.linkedin.com/in/kanmanus-ongvisatepaiboon-71556917" target="_blank" className="contact-item">
                  <img src="images/contact-icon-linkedin.png" alt="LinkedIn"/>
                </a>
              </TweenOne>
            </div>
          </div>
        </Parallax>
      </Element>
    );
  }
}

module.exports = Contact;
