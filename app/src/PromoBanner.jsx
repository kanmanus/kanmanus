import React from 'react';
import {Parallax} from 'react-parallax';
import ScrollAnim from 'rc-scroll-anim';
import TweenOne from 'rc-tween-one';

var Element = ScrollAnim.Element;

require('../scss/promo-banner.scss');

class PromoBanner extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      content: []
    }
  }
  componentDidMount() {
    var promoText = [];
    promoText.push(
      <div id="promo-text" key={0}>
        <TweenOne key="0" animation={{ y: 0, opacity: 1, delay: 100, duration: 600 }} style={{ transform: 'translateY(100px)', opacity: 0 }}>
          <div className="main-text">I DELIVER PROFESSIONAL WEBSITES</div>
        </TweenOne>

        <TweenOne key="1" animation={{ y: 0, opacity: 1, delay: 500, duration: 600 }} style={{ transform: 'translateY(100px)', opacity: 0 }}>
          <div className="sub-text">WITH THE LATEST TECHNOLOGIES</div>
        </TweenOne>
      </div>
    );

    var component = this;
    setTimeout(function(){
      component.setState({
        content: promoText
      })
    }, 5000);
  }

  render() {
    return (
      <Element scrollName="home" id="promo-banner">
        <Parallax bgImage="images/main-banner.jpg" strength={400}>
          {this.state.content}
        </Parallax>
      </Element>
    );
  }
}

module.exports = PromoBanner;
