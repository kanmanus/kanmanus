import React from 'react';
import {Parallax} from 'react-parallax';
import ScrollAnim from 'rc-scroll-anim';
import TweenOne from 'rc-tween-one';

var Element = ScrollAnim.Element;
var ScrollOverPack = ScrollAnim.OverPack;

require('../scss/skills.scss');

class Skills extends React.Component {
  get skillGroups() {
    return  [
              {
                groupName: 'Front-end Development',
                skills: [
                  {
                    skillName: 'HTML / CSS / jQuery',
                    skillLevel: '90'
                  },
                  {
                    skillName: 'ReactJS',
                    skillLevel: '50'
                  }
                ]
              },
              {
                groupName: 'Back-end Development',
                skills: [
                  {
                    skillName: 'PHP / Laravel',
                    skillLevel: '80'
                  }
                ]
              },
              {
                groupName: 'Mobile App Development',
                skills: [
                  {
                    skillName: 'Android',
                    skillLevel: '60'
                  }
                ]
              },
              {
                groupName: 'Design',
                skills: [
                  {
                    skillName: 'Sketch',
                    skillLevel: '80'
                  },
                  {
                    skillName: 'Photoshop',
                    skillLevel: '80'
                  }
                ]
              }
            ]
  }
  render() {
    return (
      <Element scrollName="skills" id="skills">
        <Parallax bgImage="images/skills-banner.jpg" strength={400}>
          <ScrollOverPack always={false}>
            <SkillsContent skillGroups={this.skillGroups} className="text" />
          </ScrollOverPack>

          <SkillsContent skillGroups={this.skillGroups} id="skills-shadow" className="shadow text" />
        </Parallax>
      </Element>
    );
  }
}

module.exports = Skills;

class SkillsContent extends React.Component {
  render() {
    return (
      <div id={this.props.id} className={this.props.className}>
        <TweenOne animation={{ y: 0, opacity: 1, duration: 600 }} style={{ transform: 'translateY(100px)', opacity: 0 }} className="headline column is-half-tablet">
          Skills and Experience
        </TweenOne>

        <div className="skill-groups column is-half-tablet">
          <TweenOne animation={{ x: 0, opacity: 1, duration: 600 }} style={{ transform: 'translateX(100px)', opacity: 0 }}>
            <div className="headline">Skills and Experience</div>
            {this.props.skillGroups.map(function(skillGroup, index){
              return (
                <SkillGroup skillGroup={skillGroup} key={index} />
              )
            })}
          </TweenOne>
        </div>
      </div>
    )
  }
}

class SkillGroup extends React.Component {
  render() {
    return (
      <div className="skill-group-item">
        <div className="group-name">{this.props.skillGroup.groupName}</div>
        <div className="skills">
          {this.props.skillGroup.skills.map(function(skill, index){
            return (
              <Skill name={skill.skillName} level={skill.skillLevel} key={index} />
            )
          })}
        </div>
      </div>
    );
  }
}

class Skill extends React.Component {
  render() {
    return (
      <div className="skill-item">
        <TweenOne animation={{ width: this.props.level + '%', delay: 1000, duration: 600 }} className="skill-name">
          {this.props.name}
        </TweenOne>
        <div className="skill-level">{this.props.level}%</div>
      </div>
    );
  }
}
