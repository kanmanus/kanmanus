import React from 'react';
import ScrollAnim from 'rc-scroll-anim';
import TweenOne from 'rc-tween-one';

var Element = ScrollAnim.Element;
var ScrollOverPack = ScrollAnim.OverPack;

require('../scss/profile.scss');

class Profile extends React.Component {
  get highlights() {
    return  [
              {
                highlightIcon: 'images/highlight-design-icon.png',
                highlightName: 'Modern Design',
                highlightDetail: 'A great design is very important as it can make a great first impression to clients. '
                                + 'I always consider how it looks as secondary of how it works because I strongly believe that '
                                + 'when people have a good experience, they will put their trust in you and be happy with your business.<br/><br/>'
                                + 'My designs are based on modern tools and frameworks to give you a beautiful and easy-to-use website.'
              },
              {
                highlightIcon: 'images/highlight-business-icon.png',
                highlightName: 'Business Solution',
                highlightDetail: 'Today technology can drive your business to be more powerful. '
                                + 'I\'m happy to help you figure out how to deliver IT solutions to meet your business requirements.<br/><br/>'
                                + 'Fortunately, I\'m an all-in-one web developer with many years of experience in both front and back-end development, so you don\'t have to look for anyone else.'
              },
              {
                highlightIcon: 'images/highlight-marketing-icon.png',
                highlightName: 'Digital Marketing',
                highlightDetail: '<span class="italic">"In order for the light to shine so brightly, the darkness must be present. (Francis Bacon)"</span><br/><br/>'
                                + 'Nowadays, digital marketing is an important key to drive your business to success. '
                                + 'I\'m ready to find the darkness and make your business shine as a bright star.'
              }
           ]
  }

  render() {
    return (
      <Element scrollName="profile" id="profile">
        <ScrollOverPack always={false}>
          <TweenOne animation={{ y: 0, opacity: 1, duration: 600 }} style={{ transform: 'translateY(-100px)', opacity: 0 }}>
            <img src="images/avatar.png" className="profile-img" alt="Kanmanus Ongvisatepaiboon" />
          </TweenOne>

          <TweenOne animation={{ y: 0, opacity: 1, delay: 600, duration: 600 }} style={{ transform: 'translateY(100px)', opacity: 0 }}>
            <div className="fullname"><span className="bold">KANMANUS</span> ONGVISATEPAIBOON</div>
          </TweenOne>

          <TweenOne animation={{ y: 0, opacity: 1, delay: 800, duration: 600 }} style={{ transform: 'translateY(100px)', opacity: 0 }}>
            <div className="nickname">DON'T WORRY, JUST CALL ME <span className="bold">'OP'</span></div>
          </TweenOne>

          <TweenOne animation={{ y: 0, opacity: 1, delay: 1000, duration: 600 }} style={{ transform: 'translateY(100px)', opacity: 0 }}>
            <div className="topic-hr"></div>
          </TweenOne>

          <TweenOne animation={{ y: 0, opacity: 1, delay: 1200, duration: 600 }} style={{ transform: 'translateY(100px)', opacity: 0 }}>
            <div className="about-me light">
              I'm <span className="bold">Op</span>, a 25-year-old Thai freelance front-end developer,
              who loves to develop professional websites with the latest web technologies available.
            </div>
          </TweenOne>

          <div className="highlights tile">
            {this.highlights.map(function(highlightItem, index){
              return <HighlightItem highlight={highlightItem} key={index} reactKey={index} />
            })}
          </div>
        </ScrollOverPack>

        <ProfileShadow highlights={this.highlights} />
      </Element>
    );
  }
}

module.exports = Profile;

class HighlightItem extends React.Component {
  render() {
    let style = {};
    let animation = {};

    if (this.props.reactKey == 0){
      style = { transform: 'translate(-100px, 100px)', opacity: 0 }
      animation = { x: 0, y: 0, opacity: 1, delay: 500, duration: 600 }
    }
    else if (this.props.reactKey == 2){
      style = { transform: 'translate(100px, 100px)', opacity: 0 }
      animation = { x: 0, y: 0, opacity: 1, delay: 500, duration: 600 }
    }
    else{
      style = { transform: 'translateY(100px)', opacity: 0 }
      animation = { y: 0, opacity: 1, delay: 500, duration: 600 }
    }

    return (
      <div className="highlight-item tile is-parent is-4">
        <TweenOne animation={animation} style={style} className="highlight-box">
          <div className="highlight-icon">
            <img src={this.props.highlight.highlightIcon} alt={this.props.highlight.highlightName} />
          </div>
          <div className="highlight-name">{this.props.highlight.highlightName}</div>
          <div className="highlight-detail" dangerouslySetInnerHTML={{ __html: this.props.highlight.highlightDetail }}></div>
        </TweenOne>
      </div>
    );
  }
}

class ProfileShadow extends React.Component {
  render() {
    return (
      <div className="shadow" id="profile-shadow">
        <img src="images/avatar.png" className="profile-img" alt="Kanmanus Ongvisatepaiboon" />
        <div className="fullname"><span className="bold">KANMANUS</span> ONGVISATEPAIBOON</div>
        <div className="nickname">DON'T WORRY, JUST CALL ME <span className="bold">'OP'</span></div>

        <div className="topic-hr"></div>

        <div className="about-me light">
          I'm <span className="bold">Op</span>, a 25-year-old Thai freelance front-end developer,
          who loves to develop professional websites with the latest web technologies available.
        </div>

        <div className="highlights tile">
          {this.props.highlights.map(function(highlightItem, index){
            return <HighlightItemShadow highlight={highlightItem} key={index} reactKey={index} />
          })}
        </div>
      </div>
    )
  }
}

class HighlightItemShadow extends React.Component {
  render() {
    return (
      <div className="highlight-item tile is-parent is-4">
        <div className="highlight-box">
          <div className="highlight-icon">
            <img src={this.props.highlight.highlightIcon} alt={this.props.highlight.highlightName} />
          </div>
          <div className="highlight-name">{this.props.highlight.highlightName}</div>
          <div className="highlight-detail" dangerouslySetInnerHTML={{ __html: this.props.highlight.highlightDetail }}></div>
        </div>
      </div>
    )
  }
}
