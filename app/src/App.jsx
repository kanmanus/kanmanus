import React from 'react';
import Scroll from 'react-scroll';
import Loading from 'react-loading';

import Header from './Header';
import SideBar from './SideBar';
import PromoBanner from './PromoBanner';
import Profile from './Profile';
import Skills from './Skills';
import Works from './Works';
import Blogs from './Blogs';
import Contact from './Contact';

require('../scss/global/_constants.scss');
require('../scss/global/global.scss');
require('../scss/global/header.scss');

var DirectLink = Scroll.DirectLink;
var Events = Scroll.Events;
var scroll = Scroll.animateScroll;
var scrollSpy = Scroll.scrollSpy;

var durationFn = function(deltaTop) {
  return deltaTop;
};

class App extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      isLoading: true,
      isHideLoading: false,
      content: [],
      isShowSideBar: false
    }

    // Preload images
    let images = [
      'logo.png',
      'main-banner.jpg',
      'avatar.png',
      'highlight-design-icon.png',
      'highlight-business-icon.png',
      'highlight-marketing-icon.png',
      'skills-banner.jpg',
      'work-loft.jpg',
      'work-rakluke.jpg',
      'work-paradisepark.jpg',
      'work-scgheim.jpg',
      'work-digital-otop.jpg',
      'work-snap.jpg',
      'work-waggingtailbkk.jpg',
      'work-naturaltale.jpg',
      'work-screen-loft.png',
      'work-screen-rakluke.png',
      'work-screen-paradisepark.png',
      'work-screen-scgheim.png',
      'work-screen-digital-otop.png',
      'work-screen-snap.png',
      'work-screen-waggingtailbkk.png',
      'work-screen-naturaltale.png'
    ];

    images.map(function(imageObject, index) {
      let image = new Image()
      image.src = 'images/' + imageObject
      return image
    })
  }

  componentDidMount() {
    // ScrollEvent
    Events.scrollEvent.register('begin');
    Events.scrollEvent.register('end');

    scrollSpy.update();

    // Append content elements after 5 sec timeout
    var component = this;

    var contentElements = [];
    contentElements.push(<Profile key={contentElements.length} />);
    contentElements.push(<Skills key={contentElements.length} />);
    contentElements.push(<Works key={contentElements.length} />);
    contentElements.push(<Blogs key={contentElements.length} />);
    contentElements.push(<Contact key={contentElements.length} />);

    setTimeout(function(){
      component.setState({ isLoading: false, content: contentElements})

      setTimeout(function(){
        component.setState({ isHideLoading: true })
      }, 1000);
    }.bind(component), 5000);
  }

  componentWillUnmount() {
    Events.scrollEvent.remove('begin');
    Events.scrollEvent.remove('end');
  }

  handleNavToggleClick() {
    this.setState({
      isShowSideBar: !this.state.isShowSideBar
    });
  }

  handleSideBarNav() {
    this.setState({
      isShowSideBar: false
    });
  }

  handleSectionFocus(i) {
    var shadow = document.getElementById(i.location + '-shadow');

    if (shadow){
      if (i.location == 'skills')
        document.getElementById(i.location).getElementsByClassName('react-parallax')[0].getElementsByClassName('react-parallax-content')[0].removeChild(shadow);
      else
        document.getElementById(i.location).removeChild(shadow);
    }
  }

  render() {
    return (
      <div id="app-content">
        <Header
          toggleEvent={this.handleNavToggleClick.bind(this)}
          focusEvent={this.handleSectionFocus.bind(this)}
          style={{ transform: (this.state.isShowSideBar?'translateX(-300px)':'translateX(0)') }} />

        <div id="content" style={{ transform: (this.state.isShowSideBar?'translateX(-300px)':'translateX(0)') }}>
          <PromoBanner />

          {this.state.content}
        </div>

        <SideBar
          isShow={this.state.isShowSideBar}
          sideBarNavEvent={this.handleSideBarNav.bind(this)}
          toggleEvent={this.handleNavToggleClick.bind(this)}
          focusEvent={this.handleSectionFocus.bind(this)} />
        <LoadingContainer id="loading-container" isLoading={this.state.isLoading} isHideLoading={this.state.isHideLoading} />
      </div>
    );
  }
}

export default App;

class LoadingContainer extends React.Component {
  render() {
    return (
      <div id="loading" className={this.props.isLoading?'':'hide'} style={{ display: !this.props.isHideLoading?'flex':'none' }}>
        <Loading type='bubbles' color='#E29820' />
      </div>
    )
  }
}
